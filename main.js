let firstName = prompt("Enter your name", '');
let age = prompt("Enter your age", '');

// Checking the correctness of the entered data
while (firstName == null || firstName == '' || Number(firstName) || !Number(age)) {
    
    if(firstName == null) firstName = '';      //Чтобы "null" не отображалось в поле после нажатия "Отмена"
    if(age == null) age = '';               //Чтобы "null" не отображалось в поле после нажатия "Отмена"
    firstName = prompt("Please enter your name", firstName);
    age = prompt("Please enter your age", age);
}

// Fulfillment of entry conditions
if (age<18) {
    alert("You are not allowed to visit this website");  
} else if (age>=18 && age<=22) {
    let ageVerif = confirm("Are you sure you want to continue?");
    if (ageVerif) {
        confirm("Welcome " + firstName);
    }else {
        alert("You are not allowed to visit this website");
    }
}else {
    alert("Welcome, " + firstName);
}
